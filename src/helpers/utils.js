import { AUTH, COINS, EXCHANGE_RATE } from './constants';
import { get } from './apiClient';
import { types } from '../store/reducer';

export const CRYPTO_COMPARE = process.env.REACT_APP_CRYPTO_COMPARE;
export const COINGECO = process.env.REACT_APP_COINGECO;
export const STORMGAIN = process.env.REACT_APP_STORMGAIN;

export const interval = ({ dispatch }) => {
  getAllCryptos({ dispatch });
  setInterval(function () {
    getAllCryptos({ dispatch });
  }, 15000);
};

export const _sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

export const hasEmptyValues = (form) => Object.values(form).some((x) => x === null || x === '');

export const getUser = () => JSON.parse(localStorage.getItem(AUTH));

export async function login({ form, history, dispatch }) {
  await dispatch({ type: types.LOADING });
  await getAllCryptos({ dispatch });
  await _sleep(800);
  await dispatch({ type: types.LOGIN, payload: { user: form } });
  localStorage.setItem(AUTH, JSON.stringify(form));
  history.push('/comparator');
}

export async function logout({ history, dispatch }) {
  await dispatch({ type: types.LOADING });
  await _sleep(800);
  localStorage.removeItem(AUTH);
  await dispatch({ type: types.LOGOUT });
  history.push('/home');
}

/*==============================================
=       FORMAT THE CRYPTO API REQUEST         =
=============================================*/
export async function getCrypto(exchange, url) {
  const { status, data } = await get({ url: `${exchange}${url}` });
  if (status === 200) return data;
}

export async function getAllCryptos({ dispatch }) {
  const cryptoCompare = await getCrypto(CRYPTO_COMPARE, '/pricemulti?fsyms=BTC,ETH,XRP&tsyms=USD');
  const coingecoRes = await getCrypto(
    COINGECO,
    '/simple/price?ids=bitcoin,ethereum,ripple&vs_currencies=usd'
  );
  const stormgainSummary = await getCrypto(STORMGAIN, '/spot/summary');

  const coingeco = sort(convertCoingeco(coingecoRes));
  const crypto_compare = sort(convertCryptoCompare(cryptoCompare));
  const stormgain = sort(createCoinArray(filter(stormgainSummary, COINS)));
  await dispatch({
    type: types.GET_CRYPTOS,
    payload: [
      [crypto_compare, 'Crypto Compare'],
      [stormgain, 'Stormgain'],
      [coingeco, 'Coingeco'],
    ],
  });
}

/* format for cryptoCompare */
const convertCryptoCompare = (obj) =>
  Object.keys(obj).map((key) => ({
    name: key.replace('_', ' '),
    price: convertToFloat(round(obj[key].USD)),
  }));

/* format for coingeco */
const convertCoingeco = (obj) => {
  return Object.entries(obj).map((item) => {
    const name = COINS.find((n) => n.name === item[0]);
    return {
      name: name.base,
      price: convertToFloat(round(item[1].usd)),
    };
  });
};

/* format for stormgain */
const filter = (apiResponse, array) => apiResponse.filter((item) => findCoins(array, item));
const findCoins = (data, item) =>
  data.map((item) => item.base).some((coin) => comparisson(item, coin));
const comparisson = (item, y) => item.base_currency === y && item.trading_pairs !== 'BTC_USDT';
const createCoinArray = (data) => data.map((item) => singleCoin(item));
const singleCoin = (x) => ({
  name: x.base_currency,
  price: convertToFloat(round(x.last_price)),
});
const sort = (x) => x.sort((a, b) => (a.name > b.name ? 1 : -1));
const round = (x) => (x * EXCHANGE_RATE).toFixed(4);
const convertToFloat = (x) => parseFloat(x);

/*==============================================
=             CONVERT MXN TO CRYPTO            =
==============================================*/

export const convertMXNToCRYPTO = (cryptos, mxn) =>
  cryptos.map((item) =>
    item[0].map((coin) => ({
      ...coin,
      amount: formatCurrency(mxn / coin.price),
    }))
  );

/*==============================================
=          format currency to locale           =
==============================================*/
export function formatCurrency(mxn) {
  const dollarUSLocale = Intl.NumberFormat('es-MX', {
    style: 'currency',
    currency: 'MXN',
    useGrouping: true,
    maximumSignificantDigits: 4,
  });
  return dollarUSLocale.format(mxn);
}
