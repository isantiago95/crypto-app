export const AUTH = 'AUTH';
export const COINS = [
  {
    base: 'BTC',
    name: 'bitcoin',
  },
  {
    base: 'ETH',
    name: 'ethereum',
  },
  {
    base: 'XRP',
    name: 'ripple',
  },
];
export const EXCHANGE_RATE = 20.5;
