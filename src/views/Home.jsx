import React from 'react';
import Card from '../components/Card';
import WelcomeForm from '../components/WelcomeForm';
import { login } from '../helpers/utils';
import { useDispatch } from '../store/StoreProvider';

const Home = ({ history }) => {
  const dispatch = useDispatch();

  const handleSubmit = (form) => {
    login({ form, dispatch, history, path: '/comparator' });
  };

  return (
    <Card>
      <WelcomeForm login={handleSubmit} />
    </Card>
  );
};

export default Home;
