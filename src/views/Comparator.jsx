import React from 'react';
import Card from '../components/Card';
import Table from '../components/Table';
import { useStore, useDispatch } from '../store/StoreProvider';
import { logout, convertMXNToCRYPTO } from '../helpers/utils';

const Comparator = ({ history }) => {
  const { cryptos, user } = useStore();
  const dispatch = useDispatch();
  const [conversor, setConversor] = React.useState(undefined);
  const [currencies, setCurrencies] = React.useState([]);

  const _handleLogout = async () => {
    await logout({ history, dispatch });
  };

  const convert = (e) => {
    const mxn = parseFloat(e.target.value);
    setConversor(mxn);
    if (mxn > 0) setCurrencies(convertMXNToCRYPTO(cryptos, mxn));
    else setCurrencies([]);
  };

  return (
    <React.Fragment>
      <div className='user-info'>
        <h2>{`${user.name} ${user.lastName}`}</h2>
        <ul>
          <li>
            <strong>Email:</strong> {user.email}
          </li>
          <li>
            <strong>Phone:</strong> {user.phone}
          </li>
        </ul>
      </div>
      <Card>
        <input
          type='number'
          onChange={convert}
          placeholder='Convert MXN to cryto'
          value={conversor}
          style={{ marginBottom: '2em' }}
        />
        <div className='comparator-tables'>
          {cryptos &&
            cryptos.map((item, key) => (
              <Table exchange={item[0]} name={item[1]} conversion={currencies[key]} key={key} />
            ))}
        </div>
        <input className='btn' type='button' value='Back' onClick={_handleLogout} />
      </Card>
    </React.Fragment>
  );
};

export default Comparator;
