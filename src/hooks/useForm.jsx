import React from 'react';

export const useForm = (form) => {
  const [state, setState] = React.useState(form);

  const updateState = ({ value, name }) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const onChange = ({ value, name }) => updateState({ value, name });

  return {
    ...state,
    form: state,
    onChange,
  };
};
