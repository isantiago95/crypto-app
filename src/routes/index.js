import React, { Suspense } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { routes } from './routes';
import Loader from '../components/Loader';
import { useStore } from '../store/StoreProvider';
import { AUTH } from '../helpers/constants';

const AppRoute = ({ Component, Layout, isProtected, ...rest }) => {
  const { loading } = useStore();
  const auth = JSON.parse(localStorage.getItem(AUTH));
  return (
    <Route
      {...rest}
      render={(props) => {
        if (loading) return <Loader />;
        if (isProtected && !auth) return <Redirect to='/home' />;
        // authorized so return component
        return (
          <Layout>
            <Component {...props} />
          </Layout>
        );
      }}
    />
  );
};

const Routes = (props) => {
  return (
    <BrowserRouter>
      <React.Fragment>
        <Suspense fallback={<div />}>
          <Switch>
            {routes.map((route, idx) => (
              <AppRoute
                path={route.path}
                Layout={route.layout}
                Component={route.component}
                isProtected={route.isProtected}
                key={idx}
              />
            ))}
          </Switch>
        </Suspense>
      </React.Fragment>
    </BrowserRouter>
  );
};

export default Routes;
