import React from 'react';
import { Redirect } from 'react-router-dom';
import MainLayout from '../layout/MainLayout';

const Home = React.lazy(() => import('../views/Home'));
const Comparator = React.lazy(() => import('../views/Comparator'));

export const routes = [
  {
    path: '/home',
    layout: MainLayout,
    isProtected: false,
    component: Home,
  },
  {
    path: '/comparator',
    layout: MainLayout,
    isProtected: true,
    component: Comparator,
  },
  {
    path: '/',
    exact: true,
    layout: MainLayout,
    isProtected: false,
    component: () => <Redirect to='/home' />,
  },
];
