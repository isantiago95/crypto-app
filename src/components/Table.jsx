import React from 'react';
import { formatCurrency } from '../helpers/utils';

const Table = ({ exchange, conversion, name = 'Exchange' }) => (
  <div>
    <table>
      <thead>
        <tr>
          <th>{name}</th>
        </tr>
      </thead>
      <tbody>
        {exchange.map((item, key) => (
          <React.Fragment>
            <tr key={key}>
              <td key={key}>
                <div className='crypto-div'>
                  <p>{item.name}</p>
                  <div className={item.name}></div>
                </div>
                <p>{formatCurrency(item.price)}</p>
              </td>
            </tr>
          </React.Fragment>
        ))}
      </tbody>
    </table>
    <ul>
      {conversion &&
        conversion.map((coin, key) => <li key={key}>{`${coin.amount} - ${coin.name}`}</li>)}
    </ul>
  </div>
);

export default Table;
