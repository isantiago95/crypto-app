import React from 'react';

const Loader = () => (
  <div className='main-loader'>
    <div className='loading'>
      <div className='dot' />
      <div className='dot' />
      <div className='dot' />
      <div className='dot' />
      <div className='dot' />
    </div>
  </div>
);

export default Loader;
