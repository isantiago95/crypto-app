import React from 'react';
import { useForm } from '../hooks/useForm';
import { hasEmptyValues } from '../helpers/utils';

const WelcomeForm = ({ login }) => {
  const { name, lastName, email, phone, onChange, form } = useForm({
    name: '',
    lastName: '',
    email: '',
    phone: '',
  });
  const [error, setError] = React.useState(null);

  const _handleSubmit = (e) => {
    e.preventDefault();
    if (hasEmptyValues(form)) setError('All fields are required.');
    else {
      login(form);
      setError(null);
    }
  };

  return (
    <form onSubmit={_handleSubmit}>
      <h1 className='mb-1 text-center'>Crypto App</h1>
      <div className='form-group inline'>
        <div>
          <label className='title'>Name</label>
          <input
            type='text'
            id='name'
            name='name'
            placeholder='John'
            value={name}
            onChange={({ target }) => onChange(target, 'name')}
          />
          <label htmlFor='name'>First</label>
        </div>
        <div>
          <input
            type='text'
            id='lastName'
            name='lastName'
            placeholder='Doe'
            value={lastName}
            onChange={({ target }) => onChange(target, 'lastName')}
          />
          <label htmlFor='lastName'>Last</label>
        </div>
      </div>
      <div className='form-group'>
        <label htmlFor='email'>Email</label>
        <input
          type='text'
          id='email'
          name='email'
          placeholder='power.ranger@gmail.com'
          value={email}
          onChange={({ target }) => onChange(target, 'email')}
        />
      </div>
      <div className='form-group'>
        <label htmlFor='phone'>Phone Number</label>
        <input
          type='number'
          id='phone'
          name='phone'
          placeholder='Phone'
          value={phone}
          onChange={({ target }) => onChange(target, 'phone')}
        />
      </div>
      <input className='btn' type='submit' value='Submit' />

      {error && <p className='text-danger'>{error}</p>}
    </form>
  );
};

export default WelcomeForm;
