export const types = {
  LOADING: 'LOADING',
  RELOAD_PAGE: 'RELOAD_PAGE',
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
  GET_CRYPTOS: 'GET_CRYPTOS',
};

export const initialState = {
  loading: false,
};

const storeReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOADING: {
      return {
        ...state,
        loading: true,
      };
    }
    case types.RELOAD_PAGE: {
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    }
    case types.LOGIN: {
      const { user } = action.payload;
      return {
        ...state,
        user,
        loading: false,
      };
    }
    case types.LOGOUT: {
      delete state.user;
      delete state.cryptos;
      return {
        ...state,
        loading: false,
      };
    }
    case types.GET_CRYPTOS: {
      const [crypto_compare, stormgain, coingeco] = action.payload;
      return {
        ...state,
        cryptos: [crypto_compare, stormgain, coingeco],
      };
    }
    default:
      return state;
  }
};

export default storeReducer;
