import React, { createContext, useContext, useReducer } from 'react';
import storeReducer, { initialState } from './reducer';

export const StoreContext = createContext();

export const useStore = () => useContext(StoreContext)[0];
export const useDispatch = () => useContext(StoreContext)[1];

const StoreProvider = ({ children }) => {
  // const [store, dispatch] = useReducer(storeReducer, initialState);

  return (
    <StoreContext.Provider value={useReducer(storeReducer, initialState)}>
      {children}
    </StoreContext.Provider>
  );
};

export default StoreProvider;
