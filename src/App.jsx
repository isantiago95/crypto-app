import React from 'react';
import Routes from './routes/';
import { useDispatch } from './store/StoreProvider';
import { interval } from './helpers/utils';
import { types } from './store/reducer';
import { getUser } from './helpers/utils';

function App() {
  const dispatch = useDispatch();

  React.useEffect(() => {
    setUser();
    interval({ dispatch });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const setUser = () => {
    const user = getUser();
    if (user) dispatch({ type: types.RELOAD_PAGE, payload: user });
  };

  return <Routes />;
}

export default App;
