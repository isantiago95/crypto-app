# Note:

_In order to properly run the app in local, you need to create a .env file in the root with the
following values:_

- REACT_APP_CRYPTO_COMPARE="https://min-api.cryptocompare.com/data"
- REACT_APP_COINGECO="https://api.coingecko.com/api/v3"
- REACT_APP_STORMGAIN="https://public-api.stormgain.com/api/v1"

---

## Public URL

- The project is available online at:
  [Crypto Comparator](http://crypto-comparator.israelsantiago.com/home).

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for
more information.
